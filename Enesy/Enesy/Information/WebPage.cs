﻿using System.ComponentModel;

namespace Enesy
{
    public class Page
    {
        // Home page
        public const string HomePage = "http://enesy.vn";

        // Enesy CAD page
        public const string CadPage = "http://enesy.vn";

        // Fan page
        public const string FanPage = "https://www.facebook.com/hkscd/";

        // Youtube page of EnesyCAD
        public const string CadYoutube = "https://www.youtube.com/enesycadteam";
    }
}
